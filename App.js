


import Expo from 'expo';
import React from 'react';
import { View, Image, Dimensions, StyleSheet } from 'react-native';
import { Tabs, Tab, Icon, Header } from 'react-native-elements'

import Profile from './components/Profile'
import Home from './components/Home'


export default class App extends React.Component {
  constructor() {
    console.disableYellowBox = true;
    super()
    this.state = {
      selectedTab: 'Home',
    }
  }

  changeTab(selectedTab) {
    this.setState({ selectedTab })
  }

  render() {
    const { selectedTab } = this.state
    return (
      <Tabs tabBarStyle={{ backgroundColor: '#393259'}}>
        <Tab
          titleStyle={{ fontWeight: 'bold', fontSize: 10 }}
          selectedTitleStyle={{ marginTop: -1, marginBottom: 6, color: '#54b1e4' }}
          selected={selectedTab === 'Home'}
          title="Home"
          renderIcon={() => <Icon containerStyle={{ justifyContent: 'center', alignItems: 'center', marginTop: 12 }} color={'#5e6977'} name='whatshot' size={33} />}
          renderSelectedIcon={() => <Icon color={'#54b1e4'} name='whatshot' size={30} />}
          onPress={() => this.changeTab('Home')}>
          <Home />
        </Tab>
        <Tab
          titleStyle={{ fontWeight: 'bold', fontSize: 10 }}
          selectedTitleStyle={{ marginTop: -1, marginBottom: 6, color: '#54b1e4' }}
          selected={selectedTab === 'profile'}
          title="Profile"
          renderIcon={() => <Icon containerStyle={{ justifyContent: 'center', alignItems: 'center', marginTop: 12 }} color={'#5e6977'} name='person' size={33} />}
          renderSelectedIcon={() => <Icon color={'#54b1e4'} name='person' size={30} />}
          onPress={() => this.changeTab('profile')}>
          <Profile />
        </Tab>
      </Tabs>
    );
  }
}

