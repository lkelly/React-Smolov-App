import React, { Component } from 'react';
import { StyleSheet, Text, View, StatusBar } from 'react-native';
import {Header} from 'react-native-elements';
import ExercisesList from './ExercisesList'

class Home extends Component {
    render() {
        
        return (
            <View style={styles.exList}>
           
            
               
    
               <ExercisesList />
               <Header
                   statusBarProps={{ barStyle: 'light-content' }}
                    leftComponent={{ icon: 'menu', color: '#fff' }}
                    centerComponent={{ text: 'Smolov Calculator', style: { color: '#fff', fontSize: 18 } }}
                   outerContainerStyles={{ backgroundColor: '#342d4c', borderBottomWidth: 2,
                       borderBottomColor: '#2b273a' }}
                    
               />
            </View>
           
        );
    }
}


export default Home;


const styles = StyleSheet.create({
    exList: {
        alignSelf: 'stretch',
        flex: 1,
        backgroundColor: '#342d4c',
        

    },
});