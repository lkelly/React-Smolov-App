import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';

class Checked extends Component {
    render() {
        const isChecked = this.props.checked;
        if (isChecked) {
            return <View style={styles.circleFull} />;
        }
        return <View style={styles.circleEmpty} />;
    
    }
}

Checked.propTypes = {
    checked: React.PropTypes.bool.isRequired,
};

export default Checked;


const styles = StyleSheet.create({
circleFull: {
    width: 30,
    height: 30,
    borderRadius: 30 / 2,
    backgroundColor: '#38c37d'
},
circleEmpty: {
        width: 30,
        height: 30,
        borderRadius: 30 / 2,
        backgroundColor: '#524779',
        borderWidth: 2,
        borderColor: '#38c37d',
    }

});