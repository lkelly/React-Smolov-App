import React, {Component} from 'react';
import { StyleSheet, Text, ScrollView, View} from 'react-native';
import Exercise from './Exercise'

class ExercisesList extends Component {
    render() {
        return (
            <ScrollView>
            <View style={styles.exList}>
            <Exercise exerciseName="Set #1" reps={10} weight={275}/>
            <Exercise exerciseName="Set #2" reps={10} weight={275} />
            <Exercise exerciseName="Set #3" reps={10} weight={275} />
            <Exercise exerciseName="Set #4" reps={10} weight={275} />
            <Exercise exerciseName="Set #5" reps={10} weight={275} />
            </View>
            </ScrollView>
        );
    }
}

export default ExercisesList;


const styles = StyleSheet.create({
    exList: {
        alignSelf: 'stretch',
        flex: 1,
        backgroundColor: '#342d4c',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 80
    },
});