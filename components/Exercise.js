import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { Card, ListItem, Button, CheckBox } from 'react-native-elements'
import PropTypes from 'prop-types';
import Checked from './Checkbox'

class Exercise extends Component {
    constructor(props) {
        super(props);
        this.state = {
            checked: false,
            visible: true,
        }
    }
    checkBoxClick = () => {
        if(this.state.checked === false){
            this.setState({ checked: true });
        }
        else{
        this.setState({ checked: false });
        }
    }
    hideExercise = () => {
        if (this.state.visible === false) {
            this.setState({ visible: true });
        }
        else {
            this.setState({ visible: false });
        }
    }
    render() {
        return (
            <View style={styles.card}>
                <View style={styles.header}>
                    <View style={styles.left}>
                    <TouchableOpacity onPress={this.hideExercise}>
                    <Text style={[styles.headerText, styles.white]}>─</Text>
                    </TouchableOpacity>
                    </View>

                    <Text style={[styles.headerText, styles.white]}>
                        {this.props.exerciseName}
                    </Text>

                    <View style={styles.right}>
                        <TouchableOpacity onPress={this.checkBoxClick} style={styles.checkbox}>
                            <Checked checked={this.state.checked} />
                        </TouchableOpacity>
                    </View>
                    


                </View>

                
                {this.state.visible ? <View> 
                <View style={styles.cardContent}>
                    <Text style={[styles.text, styles.white]}>
                        <Text style={styles.numbers}>{this.props.weight}</Text> lbs
                    </Text>
                </View>
                
                <View style={styles.line} />
                <View style={styles.circle}>
                    <Text style={[styles.circleText, styles.white]}>x</Text>
                </View>

                <View style={styles.cardContent}>
                    <Text style={[styles.text, styles.white]}>
                        <Text style={styles.numbers}>{this.props.reps}</Text> reps
                    </Text>
                </View>
                </View>
                    : null}
            </View>
            
        );
    }
}

Exercise.propTypes = {
    exerciseName: React.PropTypes.string.isRequired,
    weight: React.PropTypes.number,
    reps: React.PropTypes.number,

};

export default Exercise;


const styles = StyleSheet.create({
    card: {
        backgroundColor: '#4a4171',
        marginBottom: 20,
        alignSelf: 'stretch',
        marginLeft: 10,
        marginRight: 10,
    },
    header:{
        backgroundColor: '#524779',
        padding:15,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',

    },
    left:{
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
    },
    right:{
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    headerText:{
        fontSize: 22,
        fontWeight: 'bold',
    },
    checkbox: {
    },
    cardContent: {
        padding: 10,
        paddingTop: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    white: {
        color: '#cad6e6',
    },
    text:{
        marginBottom: 10,
        fontSize: 22,
    },
    numbers:{
        color: '#54b1e4',
        fontWeight: 'bold',
        fontSize: 28,
    },
    circle: {
        width: 22,
        height: 22,
        borderRadius: 22 / 2,
        backgroundColor: '#64578e',
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 3,
        position: 'absolute',
        top: 57


    },
    line: {
        borderWidth: 1,
        borderColor: '#423764',
    },
    circleText: {
        marginBottom: 7,
        fontSize: 16,
    },
});